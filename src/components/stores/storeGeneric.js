import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
      beers: null,
      oneBeer: null,
      page : 1,
      per_page : 12
    },
    mutations: {
      setBeers (state, allBeers) {
        state.beers = allBeers;
      },
      add(state){
        state.per_page++;
      },
      minus(state){
        if(state.per_page-1 <=0){
            state.per_page = 1;
        }
        else{

            state.per_page--;
        }
      },
      setCurrentItem (state, item){
        state.oneBeer = item;
      },
      next(state){
        state.page++;
      },
      prev(state){
        if(state.page-1<=0){
            state.page=1;
        }
        else{
            state.page--;
        }
      }
    },
    getters: {
        getBeers: (state) => state.beers,
        getpage : (state) => state.page,
        getPerPage : (state) => state.per_page

    }
})

export default store;