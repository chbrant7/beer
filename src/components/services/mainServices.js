import axios from 'axios'
import stores from './../stores/storeGeneric'

export class mainServices{
    async getAllDataEndPoint(){
        try{
            const result = await axios.get('https://api.punkapi.com/v2/beers?page='+stores.state.page+'&per_page='+ stores.state.per_page)
            console.log("Getting all the information 🍺 ");
            return result.data;
        }
        catch(error){
            console.log("Error:" + error );
        }
        
    }

}
